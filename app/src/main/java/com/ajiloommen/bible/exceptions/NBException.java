package com.ajiloommen.bible.exceptions;

/**
 * Created by ajilo on 04-03-2018.
 */

public class NBException extends Exception {
    private String message;

    public NBException (String book) {
        this.message = "Book not found: " + book;
    }

    public String getMessage() {
        return message;
    }
}
