package com.ajiloommen.bible.dataModel;

import com.ajiloommen.bible.Bible;
import com.ajiloommen.bible.exceptions.NBException;
import com.ajiloommen.bible.dataModel.interfaces.ContextCallback;

import java.util.ArrayList;

/**
 * Created by ajilo on 04-03-2018.
 */

public class Passage {
    private Verse verse;
    private ArrayList<Verse> versesInChapter;

    public Passage(int book, int chapter, int verse, ContextCallback context) {
        initPassage(book, chapter, verse, context);
    }

    public Passage(String book, int chapter, int verse, ContextCallback context) throws NBException{
        int index;
        try {
            index = Bible.indexForName(book);
            initPassage(index, chapter, verse, context);
        } catch (NBException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void initPassage(int book, int chapter, int verse, ContextCallback context) {
        Book b = new Book(book, context.getContextCallback());
        Chapter c = new Chapter(b, chapter);
        this.verse = new Verse(c, verse);
        this.versesInChapter = this.verse.getChapter().getVerses();
    }

    public Verse getVerse() throws NullPointerException{
        if (this.verse == null) {
            throw new NullPointerException();
        }
        return verse;
    }

    public ArrayList<Verse> getVersesInChapter() throws NullPointerException{
        if (this.verse == null) {
            throw new NullPointerException();
        }
        return this.versesInChapter;
    }

    public boolean hasNext() {
        if (verse.getChapter().getChapterNumber() < verse.getChapter().getBook().getNumberOfChapters()) {
            return true;
        } else if (verse.getChapter().getChapterNumber() == verse.getChapter().getBook().getNumberOfChapters()) {
            Book b = verse.getChapter().getBook();
            if (b.getIndex() >= Bible.getBooks().size()) {
                return false;
            }
        }
        return true;
    }

    public boolean hasPrevious() {
        if (verse.getChapter().getChapterNumber() > 1) {
            return true;
        } else if (verse.getChapter().getChapterNumber() == 1) {
            Book b = verse.getChapter().getBook();
            if (b.getIndex() == 1) {
                return false;
            }
        }
        return true;
    }

    public void next() {
        if (verse.getChapter().getChapterNumber() < verse.getChapter().getBook().getNumberOfChapters()) {
            Book b = verse.getChapter().getBook();
            int chapter = verse.getChapter().getChapterNumber() + 1;
            Chapter newChapter = new Chapter(b, chapter);
            this.verse = new Verse(newChapter, 1);
            this.versesInChapter = this.verse.getChapter().getVerses();
        } else if (verse.getChapter().getChapterNumber() == verse.getChapter().getBook().getNumberOfChapters()) {
            Book b = verse.getChapter().getBook();
            if (b.getIndex() >= Bible.getBooks().size()) {
                return;
            }
            b.change(1);
            Chapter c = new Chapter(b, 1);
            this.verse = new Verse(c, 1);
            this.versesInChapter = this.verse.getChapter().getVerses();
        }
    }

    public void previous() {
        if (verse.getChapter().getChapterNumber() > 1) {
            Book b = verse.getChapter().getBook();
            int chapter = verse.getChapter().getChapterNumber() - 1;
            Chapter newChapter = new Chapter(b, chapter);
            this.verse = new Verse(newChapter, 1);
            this.versesInChapter = this.verse.getChapter().getVerses();
        } else if (verse.getChapter().getChapterNumber() == 1) {
            Book b = verse.getChapter().getBook();
            if (b.getIndex() == 1) {
                return;
            }
            b.change(-1);
            int chapterNumber = b.getNumberOfChapters();
            Chapter c = new Chapter(b, chapterNumber);
            this.verse = new Verse(c, 1);
            this.versesInChapter = this.verse.getChapter().getVerses();
        }
    }
}
