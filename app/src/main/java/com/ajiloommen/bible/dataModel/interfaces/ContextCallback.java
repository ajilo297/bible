package com.ajiloommen.bible.dataModel.interfaces;

import android.content.Context;

/**
 * Created by ajilo on 04-03-2018.
 */

public interface ContextCallback {
    Context getContextCallback();
}
