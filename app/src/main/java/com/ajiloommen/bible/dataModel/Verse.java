package com.ajiloommen.bible.dataModel;

import android.content.Context;

import com.ajiloommen.bible.Bible;
import com.ajiloommen.bible.DBHelper;
import com.ajiloommen.bible.dataModel.interfaces.ContextCallback;

import java.util.HashMap;

/**
 * Created by ajilo on 04-03-2018.
 */

public class Verse {
    private Chapter chapter;
    private int verseNumber;
    private String scripture;
    private int date;

    Verse(Chapter chapter, int verseNumber) {
        this.chapter = chapter;
        this.verseNumber = verseNumber;
        HashMap<String, Object> result = new DBHelper(chapter.getBook().getContext()).getVerse(chapter, verseNumber);
        this.scripture = (String) result.get(Bible.VERSE_KEY);
        this.date = (int) result.get(Bible.DATE_KEY);
    }

    public Chapter getChapter() {
        return chapter;
    }

    public int getVerseNumber() {
        return verseNumber;
    }

    public String getScripture() {
        return scripture;
    }

    public boolean hasBookmark() {
        switch (this.date) {
            case 1:
                return true;
            default:
                return false;
        }
    }
}
