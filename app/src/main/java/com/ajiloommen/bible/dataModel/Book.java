package com.ajiloommen.bible.dataModel;

import android.content.Context;

import com.ajiloommen.bible.Bible;
import com.ajiloommen.bible.DBHelper;
import com.ajiloommen.bible.exceptions.NBException;

/**
 * Created by ajilo on 04-03-2018.
 * Book Class
 */

public class Book{
    private int index;
    private String name;
    private int numberOfChapters;
    private Context context;

    public Book(int index, Context context) {
        initWithIndex(index, context);
    }

    public Book(String bookName, Context context) throws NBException{
        int index;
        try {
            index = Bible.indexForName(bookName);
            initWithIndex(index, context);
        } catch (NBException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void initWithIndex(int index, Context context) {
        this.index = index;
        this.context = context;
        this.name = Bible.bookNameForIndex(index);
        this.numberOfChapters = new DBHelper(context).numberOfChapters(index);
    }

    public void change(int i) {
        this.index = this.index + i;
        this.name = Bible.bookNameForIndex(index);
        this.numberOfChapters = new DBHelper(context).numberOfChapters(index);
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfChapters() {
        return numberOfChapters;
    }

    Context getContext() {
        return context;
    }
}
