package com.ajiloommen.bible.dataModel;

import android.content.Context;

import com.ajiloommen.bible.DBHelper;
import com.ajiloommen.bible.dataModel.interfaces.ContextCallback;

import java.util.ArrayList;

/**
 * Created by ajilo on 04-03-2018.
 */

public class Chapter {
    private Book book;
    private int chapter;
    private int numberOfVerses;

    Chapter(Book book, int chapter) {
        this.book = book;
        this.chapter = chapter;
        this.numberOfVerses = new DBHelper(book.getContext()).numberOfVerses(book, chapter);
    }

    public Book getBook() {
        return book;
    }

    public int getChapterNumber() {
        return chapter;
    }

    public int getNumberOfVerses() {
        return numberOfVerses;
    }

    public ArrayList<Verse> getVerses() {
        ArrayList<Verse> verses = new ArrayList<>();

        int i = 0;
        while (i < this.numberOfVerses) {
            verses.add(new Verse(this, i+1));
            i += 1;
        }
        return verses;
    }

}
