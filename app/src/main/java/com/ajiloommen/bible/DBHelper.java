package com.ajiloommen.bible;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ajiloommen.bible.dataModel.Book;
import com.ajiloommen.bible.dataModel.Chapter;
import com.ajiloommen.bible.dataModel.Verse;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ajilo on 04-03-2018.
 * Class to manage Database actions
 */

public class DBHelper extends SQLiteAssetHelper{

    private static final String DATABASE_NAME = "RNKJV.bbl.mybible.db";
    private static final int DATABASE_VERSION = 1;

    private static final String BIBLE_TABLE = "Bible";
    private static final String BOOK_ID = "Book";
    private static final String CHAPTER_ID = "Chapter";
    private static final String VERSE_ID = "Verse";
    private static final String BOOKMARK_ID = "bm";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public Integer numberOfChapters(int index) {
        SQLiteDatabase db = this.getReadableDatabase();
        Integer count = null;
        String query = "SELECT * FROM " + BIBLE_TABLE + " WHERE " +
                BOOK_ID + " == " + index;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToLast();
            count = cursor.getInt(1);
            cursor.close();
        }
        db.close();
        return count;
    }

    public Integer numberOfVerses(Book book, int chapter) {
        SQLiteDatabase db = this.getReadableDatabase();
        Integer count = null;
        String query = "SELECT * FROM " + BIBLE_TABLE + " WHERE " +
                BOOK_ID + " == " + book.getIndex() + " AND " +
                CHAPTER_ID + " == " + chapter;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToLast();
            count = cursor.getInt(2);
            cursor.close();
        }
        db.close();
        return count;
    }

    public String getScripture(Chapter chapter, int verse) {
        SQLiteDatabase db = this.getReadableDatabase();
        String scripture = "";
        String query = "SELECT * FROM " + BIBLE_TABLE + " WHERE " +
                BOOK_ID + " == " + chapter.getBook().getIndex() + " AND " +
                CHAPTER_ID + " == " + chapter.getChapterNumber() + " AND " +
                VERSE_ID + " == " + verse;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            scripture = cursor.getString(3);
            cursor.close();
        }
        db.close();
        return scripture;
    }

    public HashMap<String, Object> getVerse(Chapter chapter, int verse) {
        SQLiteDatabase db = this.getReadableDatabase();
        String scripture = "";
        Integer date = null;
        String query = "SELECT * FROM " + BIBLE_TABLE + " WHERE " +
                BOOK_ID + " = " + chapter.getBook().getIndex() + " AND " +
                CHAPTER_ID + " = " + chapter.getChapterNumber() + " AND " +
                VERSE_ID + " = " + verse;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            scripture = cursor.getString(3);
            date = cursor.getInt(4);
            Log.e("COLUMN NAME", cursor.getColumnName(4));
            cursor.close();
        }
        db.close();

        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put(Bible.VERSE_KEY, scripture);
        resultMap.put(Bible.DATE_KEY, date == null ? 0 : date);

        return resultMap;
    }

    public void addBookmark(Verse v) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(BOOKMARK_ID, 1);

        String[] args = new String[3];
        args[0] = String.valueOf(v.getChapter().getBook().getIndex());
        args[1] = String.valueOf(v.getChapter().getChapterNumber());
        args[2] = String.valueOf(v.getVerseNumber());

        db.update(BIBLE_TABLE, values, BOOK_ID + " = ? AND " + CHAPTER_ID + " = ? AND " + VERSE_ID + " = ?", args);

        db.close();
    }

    public void removeBookmark(Verse v) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(BOOKMARK_ID, 0);

        String[] args = new String[3];
        args[0] = String.valueOf(v.getChapter().getBook().getIndex());
        args[1] = String.valueOf(v.getChapter().getChapterNumber());
        args[2] = String.valueOf(v.getVerseNumber());

        db.update(BIBLE_TABLE, values, BOOK_ID + " = ? AND " + CHAPTER_ID + " = ? AND " + VERSE_ID + " = ?", args);

        db.close();
    }
}
