package com.ajiloommen.bible.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ajiloommen.bible.BuildConfig;
import com.ajiloommen.bible.DBHelper;
import com.ajiloommen.bible.books.BooksActivity;
import com.ajiloommen.bible.R;
import com.ajiloommen.bible.chapter.ChapterActivity;
import com.ajiloommen.bible.dataModel.Book;
import com.ajiloommen.bible.dataModel.Passage;
import com.ajiloommen.bible.dataModel.Verse;
import com.ajiloommen.bible.dataModel.interfaces.ContextCallback;
import com.ajiloommen.bible.exceptions.NBException;

import static android.view.View.*;
import static com.ajiloommen.bible.Bible.BOOK_RESULT;
import static com.ajiloommen.bible.Bible.BOOK_RESULT_CODE;
import static com.ajiloommen.bible.Bible.CHAPTER_INTENT_KEY;
import static com.ajiloommen.bible.Bible.CHAPTER_RESULT;
import static com.ajiloommen.bible.Bible.CHAPTER_RESULT_CODE;

public class BibleActivity extends AppCompatActivity implements ContextCallback, MainInterface.MainRecyclerListener{

    private Context context;
    private Button bookBtn, chapterBtn;
    public static Passage dispPassage;
    private RecyclerView mainRecycler;
    private MainRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bible);

        context = BibleActivity.this;
        bookBtn = findViewById(R.id.bookBtn);
        chapterBtn = findViewById(R.id.chapterBtn);
        mainRecycler = findViewById(R.id.mainRecycler);

        dispPassage = new Passage(1, 1, 1, BibleActivity.this);

        findViewById(R.id.nextBtn).setVisibility(dispPassage.hasNext() ? VISIBLE : GONE);
        findViewById(R.id.prevBtn).setVisibility(dispPassage.hasPrevious() ? VISIBLE : GONE);

        refreshView();
    }

    private void refreshView() {
        Verse v;
        try {
            v = dispPassage.getVerse();
            bookBtn.setText(v.getChapter().getBook().getName());
            chapterBtn.setText(String.valueOf(v.getChapter().getChapterNumber()));
            adapter = new MainRecyclerAdapter(dispPassage, context);
            mainRecycler.setItemAnimator(new DefaultItemAnimator());
            mainRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            mainRecycler.setAdapter(adapter);

        } catch (NullPointerException e) {
            Toast.makeText(context, "Passage is null", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void reload(boolean scrollToStart) {
        if (adapter == null) return;
        adapter.passage = dispPassage;
        adapter.notifyDataSetChanged();
        Verse v = dispPassage.getVerse();
        bookBtn.setText(v.getChapter().getBook().getName());
        chapterBtn.setText(String.valueOf(v.getChapter().getChapterNumber()));

        if (scrollToStart) mainRecycler.scrollToPosition(0);
    }

    @Override
    public Context getContextCallback() {
        return context;
    }

    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.bookBtn:
                Intent bookIntent = new Intent(context, BooksActivity.class);
                startActivityForResult(bookIntent, BOOK_RESULT_CODE);
                break;
            case R.id.chapterBtn:
                Intent chapterIntent = new Intent(context, ChapterActivity.class);
                chapterIntent.putExtra(CHAPTER_INTENT_KEY, dispPassage.getVerse().getChapter().getBook().getIndex());
                startActivityForResult(chapterIntent, CHAPTER_RESULT_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case BOOK_RESULT_CODE:
                String bookName = data.getStringExtra(BOOK_RESULT);
                try {
                    Book book = new Book(bookName, BibleActivity.this);
                    if (book.getIndex() != dispPassage.getVerse().getChapter().getBook().getIndex()) {
                        dispPassage = new Passage(book.getIndex(), 1, 1, BibleActivity.this);
                        onClick(findViewById(R.id.chapterBtn));
                    }
                } catch (NBException e) {
                    Toast.makeText(context, "Invalid book name", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            case CHAPTER_RESULT_CODE:
                int chapter = data.getIntExtra(CHAPTER_RESULT, 1);
                if (chapter != dispPassage.getVerse().getChapter().getChapterNumber()) {
                    Book book = dispPassage.getVerse().getChapter().getBook();
                    dispPassage = new Passage(book.getIndex(), chapter, 1, this);
                }
        }
        reload(true);
    }

    public void navigate(View v) {
        switch (v.getId()) {
            case R.id.prevBtn:
                dispPassage.previous();
                break;
            case R.id.nextBtn:
                dispPassage.next();
                break;
        }
        reload(true);
        findViewById(R.id.nextBtn).setVisibility(dispPassage.hasNext() ? VISIBLE : GONE);
        findViewById(R.id.prevBtn).setVisibility(dispPassage.hasPrevious() ? VISIBLE : GONE);
    }

    @Override
    public void onPassageSelected(Verse v) {
        if (!v.hasBookmark()) new DBHelper(context).addBookmark(v);
        else new DBHelper(context).removeBookmark(v);
        try {
            dispPassage = new Passage(v.getChapter().getBook().getName(), v.getChapter().getChapterNumber(), v.getVerseNumber(), this);
            reload(false);
        } catch (NBException e) {
            e.printStackTrace();
        }
    }
}
