package com.ajiloommen.bible.main;

import com.ajiloommen.bible.dataModel.Passage;
import com.ajiloommen.bible.dataModel.Verse;

/**
 * Created by ajilo on 08-03-2018.
 */

interface MainInterface {
    interface MainRecyclerListener{
        void onPassageSelected(Verse v);
    }
}
