package com.ajiloommen.bible.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ajiloommen.bible.R;
import com.ajiloommen.bible.dataModel.Passage;

import java.util.ArrayList;

/**
 * Created by ajilo on 07-03-2018.
 */

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.MainRecyclerViewHolder> {
    Passage passage;
    private Context context;
    private MainInterface.MainRecyclerListener listener;

    MainRecyclerAdapter(Passage passage, Context context) {
        this.passage = passage;
        this.context = context;
        listener = (MainInterface.MainRecyclerListener) context;
    }

    @NonNull
    @Override
    public MainRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scripture, parent, false);
        return new MainRecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MainRecyclerViewHolder h, int position) {
        final int i = position + 1;
        h.chapterNumber.setText(String.valueOf(i));
        h.scriptureText.setText(passage.getVersesInChapter().get(i - 1).getScripture());
        h.scripParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPassageSelected(passage.getVersesInChapter().get(i - 1));
            }
        });
        if (passage.getVersesInChapter().get(i - 1).hasBookmark()) {
            h.scripParent.setBackgroundColor(context.getResources().getColor(R.color.light));
            h.scriptureText.setTextColor(context.getResources().getColor(R.color.dark));
            h.chapterNumber.setTextColor(context.getResources().getColor(R.color.dark));
        } else {
            h.scripParent.setBackgroundColor(context.getResources().getColor(R.color.dark));
            h.scriptureText.setTextColor(context.getResources().getColor(R.color.light));
            h.chapterNumber.setTextColor(context.getResources().getColor(R.color.light));
        }
    }

    @Override
    public int getItemCount() {
        return passage.getVersesInChapter().size();
    }

    class MainRecyclerViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout scripParent;
        TextView chapterNumber;
        TextView scriptureText;

        MainRecyclerViewHolder(View v) {
            super(v);
            chapterNumber = v.findViewById(R.id.chapterNumber);
            scriptureText = v.findViewById(R.id.scriptureText);
            scripParent = v.findViewById(R.id.scripParent);
        }
    }
}
