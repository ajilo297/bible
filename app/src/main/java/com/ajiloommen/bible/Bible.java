package com.ajiloommen.bible;

import android.app.Application;

import com.ajiloommen.bible.exceptions.NBException;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ajilo on 04-03-2018.
 */

public class Bible extends Application{

    private static final  String[] ALL_BOOKS = {
            "Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy", "Joshua", "Judges", "Ruth",
            "1 Samuel", "2 Samuel", "1 Kings", "2 Kings", "1 Chronicles", "2 Chronicles", "Ezra",
            "Nehemiah", "Esther", "Job", "Psalms", "Proverbs", "Ecclesiastes", "Song of Songs",
            "Isaiah", "Jeremiah", "Lamentations", "Ezekiel", "Daniel", "Hosea", "Joel", "Amos",
            "Obadiah", "Jonah", "Micah", "Nahum", "Habukkuk", "Zephaniah", "Haggai", "Zechariah",
            "Malachi", "Mathew", "Mark", "Luke", "John", "Acts", "Romans", "1 Corinthians",
            "2 Corinthians", "Galatians", "Ephesians", "Philippians", "Colossians",
            "1 Thessalonians", "2 Thessalonians", "1 Timothy", "2 Timothy", "Titus", "Philemon",
            "Hebrews", "James", "1 Peter", "2 Peter", "1 John", "2 John", "3 John", "Jude",
            "Revelation" };

    public static final int BOOK_RESULT_CODE = 1000;
    public static final String BOOK_RESULT = "book";

    public static final String CHAPTER_INTENT_KEY = "bookIndex";
    public static final int CHAPTER_RESULT_CODE = 1001;
    public static final String CHAPTER_RESULT = "chapter";

    public static final String VERSE_KEY = "scripture";
    public static final String DATE_KEY = "bookmark";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static String bookNameForIndex(int index) {
        return ALL_BOOKS[index - 1];
    }

    public static int indexForName(String book) throws NBException {
        int i = 0;
        while (i < ALL_BOOKS.length) {
            if (book.matches(ALL_BOOKS[i])) {
                return i +1;
            }
            i += 1;
        }
        throw new NBException(book);
    }

    public static ArrayList<String> getBooks() {
        ArrayList<String> books = new ArrayList<>();
        books.addAll(Arrays.asList(ALL_BOOKS));
        return books;
    }
}
