package com.ajiloommen.bible.chapter;

/**
 * Created by ajilo on 08-03-2018.
 */

interface ChapterInterface {

    interface ChapterSelectedListener{
        void onChapterSelected(int chapter);
    }
}
