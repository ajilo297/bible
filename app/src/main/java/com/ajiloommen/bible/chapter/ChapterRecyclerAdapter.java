package com.ajiloommen.bible.chapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ajiloommen.bible.R;
import com.ajiloommen.bible.main.BibleActivity;

/**
 * Created by ajilo on 08-03-2018.
 */

class ChapterRecyclerAdapter extends RecyclerView.Adapter<ChapterRecyclerAdapter.ChapterViewHolder>{
    private int maxChapter;
    private ChapterInterface.ChapterSelectedListener listener;

    ChapterRecyclerAdapter(int maxChapter, Context context) {
        this.maxChapter = maxChapter;
        listener = (ChapterInterface.ChapterSelectedListener) context;
    }

    @NonNull
    @Override
    public ChapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter, parent, false);
        return new ChapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChapterViewHolder h, int position) {
        final int i = position;
        h.chapterText.setText(String.valueOf(i + 1));
        if (i + 1 == BibleActivity.dispPassage.getVerse().getChapter().getChapterNumber()) {
            h.chapterText.setTypeface(h.chapterText.getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            h.chapterText.setTypeface(Typeface.DEFAULT);
        }
        h.chapterText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onChapterSelected(i + 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return maxChapter;
    }

    class ChapterViewHolder extends RecyclerView.ViewHolder {
        TextView chapterText;
        ChapterViewHolder(View v) {
            super(v);
            chapterText = v.findViewById(R.id.chapterText);
        }
    }
}
