package com.ajiloommen.bible.chapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ajiloommen.bible.R;
import com.ajiloommen.bible.dataModel.Book;

import static com.ajiloommen.bible.Bible.CHAPTER_INTENT_KEY;
import static com.ajiloommen.bible.Bible.CHAPTER_RESULT;
import static com.ajiloommen.bible.Bible.CHAPTER_RESULT_CODE;

public class ChapterActivity extends AppCompatActivity implements ChapterInterface.ChapterSelectedListener{

    private RecyclerView chapterRecycler;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);

        chapterRecycler = findViewById(R.id.chapterRecycler);
        toolbar = findViewById(R.id.toolbar);

        refreshView();
    }

    private void refreshView() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar()==null) return;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Select Chapter");

        Intent intent = getIntent();
        int bookIndex = intent.getIntExtra(CHAPTER_INTENT_KEY, 1);
        Book book = new Book(bookIndex, ChapterActivity.this);

        ChapterRecyclerAdapter adapter = new ChapterRecyclerAdapter(book.getNumberOfChapters(), this);
        chapterRecycler.setItemAnimator(new DefaultItemAnimator());
        chapterRecycler.setLayoutManager(new GridLayoutManager(this,5));
        chapterRecycler.setAdapter(adapter);
    }

    @Override
    public void onChapterSelected(int chapter) {
        Intent result = new Intent();
        result.putExtra(CHAPTER_RESULT, chapter);
        setResult(Activity.RESULT_OK, result);
        finish();
    }
}
