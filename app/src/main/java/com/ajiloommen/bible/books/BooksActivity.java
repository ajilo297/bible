package com.ajiloommen.bible.books;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ajiloommen.bible.Bible;
import com.ajiloommen.bible.R;

import static com.ajiloommen.bible.Bible.BOOK_RESULT;

public class BooksActivity extends AppCompatActivity implements BookInterface.BookRecyclerListener{

    RecyclerView bookRecycler;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        bookRecycler = findViewById(R.id.bookRecycler);
        toolbar = findViewById(R.id.toolbar);

        refreshView();
    }

    private void refreshView() {
        this.setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle("Select book");
        }

        BookRecyclerAdapter adapter = new BookRecyclerAdapter(Bible.getBooks(), BooksActivity.this);
        bookRecycler.setItemAnimator(new DefaultItemAnimator());
        bookRecycler.setLayoutManager(new LinearLayoutManager(BooksActivity.this, LinearLayoutManager.VERTICAL, false));
        bookRecycler.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onBookSelected(String bookName) {
        Intent intent = new Intent();
        intent.putExtra(BOOK_RESULT, bookName);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
