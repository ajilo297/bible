package com.ajiloommen.bible.books;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ajiloommen.bible.Bible;
import com.ajiloommen.bible.R;
import com.ajiloommen.bible.main.BibleActivity;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by ajilo on 07-03-2018.
 */

public class BookRecyclerAdapter extends RecyclerView.Adapter<BookRecyclerAdapter.BookViewHolder>{
    private ArrayList<String> booksList;
    private Context context;
    private BookInterface.BookRecyclerListener listener;

    public BookRecyclerAdapter(ArrayList<String> booksList, Context context) {
        this.booksList = booksList;
        this.context = context;
        this.listener = (BookInterface.BookRecyclerListener) context;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book_name, parent, false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder h, int position) {
        final String bookName = booksList.get(position);
        h.bookText.setText(bookName);
        if (bookName.matches(BibleActivity.dispPassage.getVerse().getChapter().getBook().getName())) {
            h.bookText.setTypeface(Typeface.DEFAULT_BOLD);
            h.bookText.setTypeface(h.bookText.getTypeface(), Typeface.BOLD_ITALIC);
        } else {
            h.bookText.setTypeface(Typeface.DEFAULT);
        }
        h.bookText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBookSelected(bookName);
            }
        });
    }

    @Override
    public int getItemCount() {
        return booksList.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder {
        TextView bookText;
        BookViewHolder(View v ) {
            super(v);
            bookText = v.findViewById(R.id.bookText);
        }
    }
}
