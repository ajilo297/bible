package com.ajiloommen.bible.books;

/**
 * Created by ajilo on 07-03-2018.
 */

interface BookInterface {
    interface BookRecyclerListener {
        void onBookSelected(String bookName);
    }
    interface BookSelectedListener{
        void onBookSelected(String bookName);
    }
}